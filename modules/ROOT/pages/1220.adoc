= 1220 Physical Payroll Check Distribution
:policy-number: 1220
:policy-title: Physical Payroll Check Distribution
:revision-date: 01/31/2024
:next-review: 01/31/2026

include::partial$policy-header.adoc[]

In accordance with the State of Georgia regulations concerning the distribution of physical payroll checks, the Georgia State Accounting Office is responsible for sending printed paper checks directly through the United States Postal Service to the DHS employee’s address on file in the Employee Self Service portal.

== Authority

* Official Code of Georgia Annotated https://law.justia.com/codes/georgia/2019/title-49/chapter-2/article-1/[(O.C.G.A) § 49-2-1] DHS created effective April 5, 2021.
* https://law.justia.com/codes/georgia/2019/title-50/chapter-5b/article-1/section-50-5b-3/[O.C.G.A § 50-5B-3 (3)], The State Accounting Officer shall prescribe the manner in which disbursements shall be made by state government organizations.

== References

https://sao.georgia.gov/sites/sao.georgia.gov/files/related_files/press_release/SAO_Communication_Direct_Deposit_Policy_Updated_062113_FINAL.pdf[The State Accounting Office Mandatory Direct Deposit Policy Update Memo]

== Applicability

This policy is applicable to all Department of Human Services divisions and offices.

== Definitions

* Off Cycle Check - A manual check that is processed outside of the normal payroll cycle.
The off-cycle check process will produce a paper check.
* Pre-Note To reduce the risk of fraud, the state of Georgia executes a Pre-Note process whenever new bank account information is reported in ESS.
SAO confirms new banks and the security of their electronic connections prior to processing the first direct deposit.
* When new bank information is reported, the first payroll payment will be made in the form of a paper check via First Class mail.
You must ensure your mailing address in ESS is accurate to ensure you received the paper paycheck timely.
* Each DHS employee is responsible for updating their address in Employee Self Services (ESS TeamWorks) to ensure successful delivery of their check.

== Responsibilities

* The Director of the Office of Financial Services (OFS) is responsible for issuing and updating procedures to implement this policy.
* All paper checks are printed by the State Print Shop and sent directly to the US Postal Service for delivery directly to the check recipient's mailing address as recorded in Employee Self Service.

== History

The Georgia State Accounting Office mandatory Direct Deposit Policy – Paper Check Process – was updated effective July 2013.

== Evaluation

This policy will be evaluated by the Office of Financial Services (OFS) annually.