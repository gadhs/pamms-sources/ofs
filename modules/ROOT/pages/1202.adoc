= 1202 Statutory and Policy Requirement of Federal Grant Award Period of Performance
:policy-number: 1202
:policy-title: Statutory and Policy Requirement of Federal Grant Award Period of Performance

include::partial$policy-header.adoc[]

The policy of the Georgia Department of Human Services (DHS) is to comply with Period of Performance requirements (2 CFR 200.309) under the Post Federal Award Requirements as stated in the Uniform Administrative Requirements, Cost Principles and Audit Requirements for Federal Awards.

DHS shall charge Federal awards only with allowable costs during the period of performance as specified in each award’s terms and conditions. This policy also holds true for all sub-recipients of DHS federal awards.

== Authority

Official Code of Georgia Annotated, https://ia800704.us.archive.org/22/items/gov.ga.ocga.2018/release77.2020.08.10/gov.ga.ocga.title.49.html#t49c02s49-2-1[(O.G.C.A) § 49-2-1], DHS created effective July 1, 2009.

== References

Code of Federal Regulations: Title 2, Subtitle A, Part 200, UNIFORM ADMINISTRATIVE REQUIREMENTS, COST PRINCIPLES, AND AUDIT REQUIREMENTS FOR FEDERAL AWARDS, Subpart D, Post Federal Award Requirement. https://www.ecfr.gov/cgi-bin/text-idx?tpl=/ecfrbrowse/Title02/2cfr200_main_02.tpl

== Applicability

All Department of Human Services divisions and programs administering and executing Federal grant awards.

== Definitions

None

== Responsibilities

The Director of the DHS Office of Financial Services (OFS) is responsible for issuing and updating procedures to implement this policy.

== History

None

== Evaluation

The effectiveness of this policy is evaluated by the Georgia Department of Audits annually and by representatives of federal agencies as they perform financial management reviews.